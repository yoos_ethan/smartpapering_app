export const bankList = [
    {
        label : '국민은행',
        value : '국민은행'
    },
    {
        label : '기업은행',
        value : '기업은행'
    },
    {
        label : '농협은행',
        value : '농협은행'
    },
    {
        label : '신한은행',
        value : '신한은행'
    },
    {
        label :'산업은행',
        value :'산업은행'
    },
    {
        label :'우체국',
        value :'우체국'
    },
    {
        label :'수협은행',
        value :'수협은행'
    },
    {
        label :'케이뱅크',
        value :'케이뱅크'
    },
    {
        label :'카카오뱅크',
        value :'카카오뱅크'
    },
    {
        label :'SC(스탠다드차타드)',
        value :'SC(스탠다드차타드)'
    },
    {
        label :'하나은행',
        value :'하나은행'
    },
    {
        label :'한국씨티은행',
        value :'한국씨티은행'
    },
    {
        label :'우리은행',
        value :'우리은행'
    },
    {
        label :'경남은행',
        value :'경남은행'
    },
    {
        label :'광주은행',
        value :'광주은행'
    },
    {
        label :'대구은행',
        value :'대구은행'
    },
    {
        label :'도이치은행',
        value :'도이치은행'
    },
    {
        label :'부산은행',
        value :'부산은행'
    },
    {
        label :'전북은행',
        value :'전북은행'
    },
    {
        label :'제주은행',
        value :'제주은행'
    },
    {
        label :'저축은행',
        value :'저축은행'
    }
    
]